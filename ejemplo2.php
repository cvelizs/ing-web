<?php

$user_agent = $_SERVER['HTTP_USER_AGENT'];
date_default_timezone_set("America/Lima");
//detectar ip
function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
       
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
   
    return $_SERVER['REMOTE_ADDR'];
}
// detectar navegador
function getBrowser($user_agent){
 if(strpos($user_agent, 'MSIE') !== FALSE)
   return 'Internet Explorer';
 elseif(strpos($user_agent, 'Trident') !== FALSE) 
   return 'Internet Explorer';
 elseif(strpos($user_agent, 'Edg') !== FALSE )
   return 'Microsoft Edge';
 elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
   return "Opera Mini";
 elseif(strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
   return "Opera";
 elseif(strpos($user_agent, 'Firefox') !== FALSE)
   return 'Mozilla Firefox';
 elseif(strpos($user_agent, 'Chrome') !== FALSE)
   return 'Google Chrome';
 elseif(strpos($user_agent, 'Safari') !== FALSE)
   return "Safari";
 else
   return 'Navegador no detectado';
}
//conexión a base de datos
$navegador = getBrowser($user_agent);
$ipusario = getRealIP();
$fechaActual = date('Y-m-d');

$mysqli = new mysqli('127.0.0.1', 'root', '', 'bdiw');
$mysqli->set_charset("utf8");
//filtrar repetidos
$res = $mysqli->query("SELECT * FROM controlip_time where ip_usuario='$ipusario' and fecha_entrada='$fechaActual' and navegador='$navegador'");

if(mysqli_num_rows($res)>0){
  echo ' <br/>';
}
//insertar en la tabla
else{ 
  $query = "INSERT INTO controlip_time VALUES ('$ipusario','$fechaActual','$navegador')";
  $mysqli->query($query);
}

$res2 = $mysqli->query("SELECT navegador from controlip_time");

$arr_navegadores = array();
//mostrar los navegadores y la frecuencia de cada uno
while($f = $res2->fetch_object()){
    array_push($arr_navegadores, $f->navegador);
}
foreach (array_count_values($arr_navegadores) as $key=>$item){
    echo "$key : $item <br>";
}

echo '<br/>'."Ip y hora de visitas y navegador: ".'<br/>'.'<br/>';

//mostrar todos los datos
$res = $mysqli->query("SELECT * FROM controlip_time");
while($f = $res->fetch_object()){
    echo $f->ip_usuario.' <br/>';
    echo $f->fecha_entrada.' <br/>';
    echo $f->navegador.' <br/>'.' <br/>';
}
?>

<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          <?php  
          // generar estadísticas, usando la base de datos
          //usar plantilla htm
          $mysqli = new mysqli('127.0.0.1', 'root', '', 'bdiw');
          $mysqli->set_charset("utf8");

          $res2 = $mysqli->query("SELECT navegador from controlip_time");

          $arr_navegadores = array();

          while($f = $res2->fetch_object()){
              array_push($arr_navegadores, $f->navegador);
          }
          foreach (array_count_values($arr_navegadores) as $key=>$item){
              echo "['".$key."',".$item."],";
              
          } ?>
        ]);

        var options = {
          title: 'Porcentaje de uso en navegadores',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="donutchart" style="width: 900px; height: 500px;"></div>
  </body>
</html>
