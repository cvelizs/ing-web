from flask import Flask, render_template, request, redirect, url_for, flash
from flask import request
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://claudia:12345678@127.0.0.1/hotel'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
ma = Marshmallow(app)

class Cliente(db.Model):
    idcliente = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    dni = db.Column(db.String(8))
    cel = db.Column(db.String(9))

    def __init__(self, name, dni, cel):
        self.name = name
        self.dni = dni
        self.cel = cel

db.create_all()

class ClienteSchema(ma.Schema):
    class Meta:
        fields = ("idcliente","name", "dni","cel")

cliente_schema = ClienteSchema()
cliente_schemas = ClienteSchema(many=True)


@app.route('/create_cliente', methods=['POST'])
def create_cliente():
    print(request.json)
    name=request.json["name"]
    dni=request.json["dni"]
    cel=request.json["cel"]

    new_cliente = Cliente(name,dni,cel)

    db.session.add(new_cliente)
    db.session.commit()

    return cliente_schema.jsonify(new_cliente)

@app.route('/get_cliente', methods=['GET'])
def get_cliente():
    all_clientes = Cliente.query.all()
    result = cliente_schemas.dump(all_clientes)
    return jsonify(result)

@app.route('/update_cliente/<ide>', methods=["PUT"])
def actualizar_elector(ide):

    name=request.json["name"]
    dni=request.json["dni"]
    cel=request.json["cel"]
    cliente_id=Cliente.query.filter_by(idcliente=ide).one()
    cliente_id.name=name
    cliente_id.dni=dni
    cliente_id.cel=cel
    db.session.commit()
    return "Actualizado correctamente"


@app.route('/delete_cliente/<ide>', methods=['DELETE'])
def eliminar_cliente(ide):
    deletecliente=Cliente.query.filter_by(idcliente=ide).one()
    db.session.delete(deletecliente)
    db.session.commit()
    return "Eliminado correctamente"


@app.route("/")
def hello_world():
    return "<p>Holitas</p>"

if __name__=="__main__":   
    app.run(port=3000, debug=True)